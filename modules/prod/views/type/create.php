<?php

/* @var $this yii\web\View */
/* @var $model app\models\Type */

$this->title = 'Create Type';
$this->params['breadcrumbs'][] = ['label' => 'Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<?= $this->render('_form', [
    'model' => $model,
]) ?>

