<?php

namespace app\modules\prod\controllers;

use naffiq\bridge\controllers\BaseAdminController;
use yii\helpers\ArrayHelper;
use yii2tech\admin\actions\Position;
use dosamigos\grid\actions\ToggleAction;

/**
 * TypeController implements the CRUD actions for [[app\models\Type]] model.
 * @see app\models\Type
 */
class TypeController extends BaseAdminController
{
    /**
     * @inheritdoc
     */
    public $modelClass = 'app\models\Type';
    /**
     * @inheritdoc
     */
    public $searchModelClass = 'app\models\TypeSearch';




    /**
     * @inheritdoc
     */
    public function actions()
    {
        return ArrayHelper::merge(
            parent::actions(),
            [
                'toggle' => [
                    'class' => ToggleAction::className(),
                    'modelClass' => 'app\models\Type',
                    'onValue' => 1,
                    'offValue' => 0
                ],
                'position' => [
                    'class' => Position::className(),
                ],
            ]
        );
    }
}
