<?php

namespace app\modules\prod\controllers;

use naffiq\bridge\controllers\BaseAdminController;
use yii\helpers\ArrayHelper;
use yii2tech\admin\actions\Position;
use dosamigos\grid\actions\ToggleAction;

/**
 * CategoryController implements the CRUD actions for [[app\models\Category]] model.
 * @see app\models\Category
 */
class CategoryController extends BaseAdminController
{
    /**
     * @inheritdoc
     */
    public $modelClass = 'app\models\Category';
    /**
     * @inheritdoc
     */
    public $searchModelClass = 'app\models\CategorySearch';




    /**
     * @inheritdoc
     */
    public function actions()
    {
        return ArrayHelper::merge(
            parent::actions(),
            [
                'toggle' => [
                    'class' => ToggleAction::className(),
                    'modelClass' => 'app\models\Category',
                    'onValue' => 1,
                    'offValue' => 0
                ],
                'position' => [
                    'class' => Position::className(),
                ],
            ]
        );
    }
}
