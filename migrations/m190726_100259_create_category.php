<?php

use yii\db\Migration;

/**
 * Class m190726_100259_create_category
 */
class m190726_100259_create_category extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m190726_100259_create_category cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190726_100259_create_category cannot be reverted.\n";

        return false;
    }
    */
}
