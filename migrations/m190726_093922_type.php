<?php

use yii\db\Migration;

/**
 * Class m190726_093922_type
 */
class m190726_093922_type extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('type', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m190726_093922_type cannot be reverted.\n";

        $this->dropTable('type');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190726_093922_type cannot be reverted.\n";

        return false;
    }
    */
}
