<?php

use yii\db\Migration;

/**
 * Class m190726_095913_type_update_fix
 */
class m190726_095913_type_update_fix extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn('type', 'title', $this->string()->notNull());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m190726_095913_type_update_fix cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190726_095913_type_update_fix cannot be reverted.\n";

        return false;
    }
    */
}
