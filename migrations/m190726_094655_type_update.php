<?php

use yii\db\Migration;

/**
 * Class m190726_094655_type_update
 */
class m190726_094655_type_update extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->update('type', [
            'title' => $this->string()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m190726_094655_type_update cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190726_094655_type_update cannot be reverted.\n";

        return false;
    }
    */
}
