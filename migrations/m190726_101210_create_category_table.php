<?php

use yii\db\Migration;

/**
 * Handles the creation of table `category`.
 * Has foreign keys to the tables:
 *
 * - `type`
 */
class m190726_101210_create_category_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('category', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'type_id' => $this->integer()->notNull(),
        ]);

        // creates index for column `type_id`
        $this->createIndex(
            'idx-category-type_id',
            'category',
            'type_id'
        );

        // add foreign key for table `type`
        $this->addForeignKey(
            'fk-category-type_id',
            'category',
            'type_id',
            'type',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `type`
        $this->dropForeignKey(
            'fk-category-type_id',
            'category'
        );

        // drops index for column `type_id`
        $this->dropIndex(
            'idx-category-type_id',
            'category'
        );

        $this->dropTable('category');
    }
}
