<?php

namespace app\controllers;

class CategoryController extends BaseController
{
    public $modelClass = 'app\models\Category';
}