<?php

namespace app\controllers;

class ProductController extends BaseController
{
    public $modelClass = 'app\models\Product';
}